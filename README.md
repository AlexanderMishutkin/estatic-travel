# CapyToursAgency

<!-- ## Already deployed

Example is deployed (here)[http://nevbros.ru:4200]. -->

## How to run?

```
npm i
ng serve
```

And open (site)[http://localhost:4200].

## Features

- Only reading from firebase
- All CRUD operations in memory (After log in only. Some deletion and adding can be done on every page, while to edit press edit "pencil" button).
- Verification of email and phone fields
- User registration and management


## Other important things

- Search is implemented, it works as inexact search over the whole word
- Ratio of card is rounded to int (w = h OR 2*w = h OR ...). It looks pretty good, when there are enougth much cards...
