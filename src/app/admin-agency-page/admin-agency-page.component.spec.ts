import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAgencyPageComponent } from './admin-agency-page.component';

describe('AdminAgencyPageComponent', () => {
  let component: AdminAgencyPageComponent;
  let fixture: ComponentFixture<AdminAgencyPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAgencyPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminAgencyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
