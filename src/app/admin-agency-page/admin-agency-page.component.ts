import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Destination, Agency, DataServiceService } from '../services/data-service.service';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-admin-agency-page',
  templateUrl: './admin-agency-page.component.html',
  styleUrls: ['./admin-agency-page.component.css']
})
export class AdminAgencyPageComponent {
  errors: string = "";
  destLink(d: Destination): string | undefined {
    return '/destination/' + d.id + '?by=' + this.agency?.id;
  }
  agency: Agency | undefined = undefined;
  emailRegex: RegExp = /.+@.+\..+/;
  phoneRegex: RegExp = /[+\d\-()/]+/;


  encodeURIComponent = encodeURIComponent;

  constructor(private route: ActivatedRoute, private data: DataServiceService, public user: UserServiceService, private router: Router) {
    this.numCols.subscribe(console.log);
    this.numCols.subscribe((n) => {
      this.columns = new Array(n).fill(null).map(() => []);
      let i = Math.floor(n / 2);
      for (let a of this.agency?.destinacije ?? []) {
        this.columns[(i++) % n].push(a);
      }
    })
  }

  ngOnInit() {
    document.documentElement.scrollTop = 0;
    const id = this.route.snapshot.paramMap.get('agencyId');
    this.agency = JSON.parse(JSON.stringify(this.data.agencies.find(a => a.id == id) ?? {}));

    this.numCols.next(Math.ceil(window.innerWidth / 500));
  }

  numCols: BehaviorSubject<number> = new BehaviorSubject(3);
  columns: Destination[][] = [];

  onResize(event: { target: { innerWidth: number; }; }) {
    console.log(this.columns)
    this.numCols.next(Math.ceil(event.target.innerWidth / 500))
  }

  deleteAgency(agency: Agency | undefined) {
    if (agency)
    this.data.deleteAgency(agency.id);
    this.router.navigate(['/agencies']);
  }

  deleteDestination(destination: Destination) {
    return ()=> {
      if (this.agency) this.agency.destinacije = this.agency.destinacije.filter(a=>a.id != destination.id);
      this.numCols.next(this.numCols.value);
    };
  }

  addDestination() {
    this.errors = "You must safe changes before adding new destination";
  }

  save() {
    if (!this.phoneRegex.test(this.agency?.brojTelefona ?? '') || !this.emailRegex.test(this.agency?.email ?? '')) {
      this.errors = "Fix email and/or phone before saving!";
    } else {
      if (this.agency) this.data.updateAgency(this.agency.id, this.agency);
      this.router.navigateByUrl((this.agency) ? ('/agency/' + this.agency.id) : '/');
    }
  }
}
