import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDestinationPageComponent } from './admin-destination-page.component';

describe('AdminDestinationPageComponent', () => {
  let component: AdminDestinationPageComponent;
  let fixture: ComponentFixture<AdminDestinationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDestinationPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminDestinationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
