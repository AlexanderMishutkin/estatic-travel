import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { Destination, Agency, DataServiceService } from '../services/data-service.service';
import { BehaviorSubject, filter, pairwise } from 'rxjs';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-admin-destination-page',
  templateUrl: './admin-destination-page.component.html',
  styleUrls: ['./admin-destination-page.component.css']
})
export class AdminDestinationPageComponent {
  destination: Destination | undefined = undefined;
  agency: Agency | undefined = undefined;
  logo: string = '';
  newSlika: string = '';

  encodeURIComponent = encodeURIComponent;

  constructor(private route: ActivatedRoute, public user: UserServiceService, private data: DataServiceService, private router: Router) {
    this.numCols.subscribe(console.log);
    this.numCols.subscribe((n) => {
      this.columns = new Array(n).fill(null).map(() => []);
      let i = Math.floor(n / 2);
      for (let a of this.destination?.slike ?? []) {
        this.columns[(i++) % n].push(a);
      }
    })

    this.route.queryParamMap.subscribe(params => {
      this.agency = this.data.agencies.find(a=> a.id == params.get('by'));
      console.log(this.agency, params.get('by'))
    });
  }

  ngOnInit() {
    document.documentElement.scrollTop = 0;
    const id = this.route.snapshot.paramMap.get('destinationId');
    this.destination = this.data.agencies.map(a => a.destinacije.find(d => {
      return d.id == id;
    })).find(d => d != undefined);
    if (this.destination) this.destination = JSON.parse(JSON.stringify(this.destination))
    if (this.destination) this.logo = this.destination.slike?.[0] ?? 'assets/capys/_929cdc23-25cc-422f-b4f6-1ccfe265d67a.jpg';

    this.numCols.next(Math.ceil(window.innerWidth / 500));

  }

  stp = (event: { stopPropagation: () => any; }) => event.stopPropagation();


  numCols: BehaviorSubject<number> = new BehaviorSubject(3);
  columns: string[][] = [];

  onResize(event: { target: { innerWidth: number; }; }) {
    console.log(this.columns)
    this.numCols.next(Math.ceil(event.target.innerWidth / 500))
  }

  deleteDestination(destination: Destination | undefined) {
    if (destination && this.agency)
    this.data.deleteDestination(destination.id, this.agency);
    this.router.navigate([this.agency ? '/agency/' + this.agency.id : '/']);
  }

  deleteSlike(link: string) {
    return ()=> {
      if (this.destination) {
        this.destination.slike = this.destination?.slike.filter(s=>s!=link);
      }
      this.numCols.next(this.numCols.value);
    };
  }

  save() {
    if (this.destination && this.agency) this.data.updateDestination(this.destination.id, this.destination, this.agency);
    console.log(this.destination && this.agency, (this.destination && this.agency) ? ('/destination/' + this.destination.id + '?by=' + this.agency.id) : '/')
    this.router.navigateByUrl((this.destination && this.agency) ? ('/destination/' + this.destination.id + '?by=' + this.agency.id) : '/');
  }

  addImage() {
    this.destination?.slike.push(this.newSlika);
    this.newSlika = '';
    this.numCols.next(this.numCols.value);
  }
}
