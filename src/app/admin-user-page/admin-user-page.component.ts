import { Component } from '@angular/core';
import { DataServiceService, User } from '../services/data-service.service';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-admin-user-page',
  templateUrl: './admin-user-page.component.html',
  styleUrls: ['./admin-user-page.component.css']
})
export class AdminUserPageComponent {
  public clicked: boolean = false;
  public curUser: User | undefined;

  constructor(public data: DataServiceService, public auth: UserServiceService) {
    
  }

  edit(user: User) {
    this.curUser = JSON.parse(JSON.stringify(user));
    console.log(user)
    this.clicked = true;
  }

  color(user: User) {
    return (user.id == this.auth.user.value?.id) ? 'lightyellow' : '';
  }
  
  stp = (event: { stopPropagation: () => any; }) => event.stopPropagation();

  editUser(user: User) {
    this.data.changeUser(user);
    this.clicked = false;
  }
}
