import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Agency, DataServiceService, Destination } from '../services/data-service.service';
import { BehaviorSubject } from 'rxjs';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-agency-page',
  templateUrl: './agency-page.component.html',
  styleUrls: ['./agency-page.component.css'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class AgencyPageComponent {
  destLink(d: Destination): string | undefined {
    return '/destination/' + d.id + '?by=' + this.agency?.id;
  }
  agency: Agency | undefined = undefined;
  opacity: number = 0;

  encodeURIComponent = encodeURIComponent;

  constructor(private route: ActivatedRoute, private data: DataServiceService, public user: UserServiceService, private router: Router) {
    this.numCols.subscribe(console.log);
    this.numCols.subscribe((n) => {
      this.columns = new Array(n).fill(null).map(() => []);
      let i = Math.floor(n / 2);
      for (let a of this.agency?.destinacije ?? []) {
        this.columns[(i++) % n].push(a);
      }
    })
  }

  ngOnInit() {
    document.documentElement.scrollTop = 0;
    setTimeout(() => {
      this.opacity = 100;
    }, 300)
    const id = this.route.snapshot.paramMap.get('agencyId');
    this.agency = this.data.agencies.find(a => a.id == id);

    this.numCols.next(Math.ceil(window.innerWidth / 500));
  }

  numCols: BehaviorSubject<number> = new BehaviorSubject(3);
  columns: Destination[][] = [];

  onResize(event: { target: { innerWidth: number; }; }) {
    console.log(this.columns)
    this.numCols.next(Math.ceil(event.target.innerWidth / 500))
  }

  deleteAgency(agency: Agency | undefined) {
    if (agency)
      this.data.deleteAgency(agency.id);
    this.router.navigate(['/agencies']);
  }

  deleteDestination(destination: Destination) {
    return () => {
      if (this.agency) this.data.deleteDestination(destination.id, this.agency);
      this.numCols.next(this.numCols.value);
    };
  }

  addDestination() {
    const randomString = '-MNQ' + Array.from(crypto.getRandomValues(new Uint8Array(16))).map(
      byte => String.fromCharCode(byte % ('Z'.charCodeAt(0) - 'A'.charCodeAt(0)) + 'A'.charCodeAt(0))
    ).join('');
    console.log(randomString)
    if (this.agency) {
      this.data.addDestination({
        id: randomString,
        naziv: "",
        opis: "",
        slike: [],
        tip: "",
        prevoz: "",
        cena: 35201,
        maxOsoba: 21
      } as Destination, this.agency);
      this.router.navigateByUrl('/admin/edit/destination/' + randomString + '?by=' + this.agency.id);
    }

  }
}
