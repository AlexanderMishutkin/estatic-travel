import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexPageComponent } from './index-page/index-page.component';
import { DestinationPageComponent } from './destination-page/destination-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminDestinationPageComponent } from './admin-destination-page/admin-destination-page.component';
import { AdminAgencyPageComponent } from './admin-agency-page/admin-agency-page.component';
import { AdminUserPageComponent } from './admin-user-page/admin-user-page.component';
import { AdminHomePageComponent } from './admin-home-page/admin-home-page.component';
import { AgencyPageComponent } from './agency-page/agency-page.component';

const routes: Routes = [
  { path: 'agencies', component: IndexPageComponent },
  { path: 'agency/:agencyId', component: AgencyPageComponent },
  { path: 'destination/:destinationId', component: DestinationPageComponent },
  { path: 'admin/edit/agency/:agencyId', component: AdminAgencyPageComponent },
  { path: 'admin/edit/destination/:destinationId', component: AdminDestinationPageComponent },
  { path: 'admin/edit/users', component: AdminUserPageComponent },
  { path: 'admin', component: AdminHomePageComponent },
  { path: '',   redirectTo: '/agencies', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
