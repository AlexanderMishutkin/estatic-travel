import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { AgencyPageComponent } from './agency-page/agency-page.component';
import { DestinationPageComponent } from './destination-page/destination-page.component';
import { HttpClientModule } from '@angular/common/http';
import { LoadingIconComponent } from './loading-icon/loading-icon.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleCardComponent } from './simple-card/simple-card.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AdminAgencyPageComponent } from './admin-agency-page/admin-agency-page.component';
import { AdminDestinationPageComponent } from './admin-destination-page/admin-destination-page.component';
import { AdminUserPageComponent } from './admin-user-page/admin-user-page.component';
import { AdminHomePageComponent } from './admin-home-page/admin-home-page.component';
import { HighlightableComponent } from './highlightable/highlightable.component';
import { FormsModule } from '@angular/forms';
import { NiceInputComponent } from './nice-input/nice-input.component';
import { UserFormComponent } from './user-form/user-form.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    AgencyPageComponent,
    DestinationPageComponent,
    LoadingIconComponent,
    SimpleCardComponent,
    NavBarComponent,
    PageNotFoundComponent,
    LoginPageComponent,
    AdminAgencyPageComponent,
    AdminDestinationPageComponent,
    AdminUserPageComponent,
    AdminHomePageComponent,
    HighlightableComponent,
    NiceInputComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
