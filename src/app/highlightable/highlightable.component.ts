import { Component, Input } from '@angular/core';
import { SearchServiceService } from '../services/search-service.service';

@Component({
  selector: 'app-highlightable',
  templateUrl: './highlightable.component.html',
  styleUrls: ['./highlightable.component.css']
})
export class HighlightableComponent {
  @Input() text: string = '';

  constructor(public search: SearchServiceService) {
  }

  yellowIfNeeded(word: string, request: any) {
    word = word.replaceAll('#', '');
    return this.search.amISimilar(word) ? 'yellow' : 'white';
  }
}
