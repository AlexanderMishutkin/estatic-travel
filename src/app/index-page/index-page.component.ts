import { Component } from '@angular/core';
import { Agency, DataServiceService } from '../services/data-service.service';
import { BehaviorSubject } from 'rxjs';
import { SearchServiceService } from '../services/search-service.service';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class IndexPageComponent {
  numCols: BehaviorSubject<number> = new BehaviorSubject(3);
  columns: Agency[][] = [];
  request: string = '';


  constructor(public data: DataServiceService, public search: SearchServiceService, private router: Router, public user: UserServiceService) {
    this.numCols.subscribe(console.log);

    this.numCols.subscribe((n)=>{
      this.columns = new Array(n).fill(null).map(()=> []);
      let i = Math.floor(n / 2);
      for (let a of this.data.agencies) {
        this.columns[(i++) % n].push(a);
      }
      console.log(this.columns[0] === this.columns[1]);
    })

    setTimeout(()=>{
      this.numCols.next(this.numCols.value);
    }, 200);
  }

  filterCol(col: Agency[], request: any) {
    let r = this.search.request.value;
    if (!r || r.length < 2) return col
    return col.filter(agency=>{
      console.log(agency.naziv, r, this.search.amISimilar(agency.naziv))
      return agency.destinacije.map(d=>d.naziv).filter(this.search.amISimilar).length > 0 || this.search.amISimilar(agency.naziv);
    });
  }

  onResize(event: { target: { innerWidth: number; }; }){
    this.numCols.next(Math.ceil(event.target.innerWidth / 500))
  }

  ngOnInit() {
    this.numCols.next(Math.ceil(window.innerWidth / 500));
  }

  randomHeight(i: number) {
    return `${Math.abs(this.numCols.value / 2 - i) * 4}vh`;
  }

  getDist(agency: Agency) {
    return agency.destinacije.map(d=>d.naziv)
  }

  agencyLink(agency: Agency) {
    return '/agency/' + encodeURIComponent(agency.id);
  }

  deleteAgency(agency: Agency) {
    return ()=> {
      this.data.deleteAgency(agency.id);
      this.numCols.next(this.numCols.value);
    };
  }

  addAgency() {
    const randomString = '-MNQ' + Array.from(crypto.getRandomValues(new Uint8Array(16))).map(
      byte => String.fromCharCode(byte % ('Z'.charCodeAt(0) - 'A'.charCodeAt(0)) + 'A'.charCodeAt(0))
    ).join('');
      this.data.addAgency({
        id: randomString,
        naziv: "",
        adresa: "",
        godina: 0,
        logo: "assets/capys/_d605491f-4727-40bf-9b5b-2902aabf5a85.jpg",
        brojTelefona: "",
        email: "",
        destinacije: []
      } as Agency);
      this.router.navigateByUrl('/admin/edit/agency/' + randomString);
  }

}
