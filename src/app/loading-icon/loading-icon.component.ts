import { Component, Output } from '@angular/core';
import { DataServiceService } from '../services/data-service.service';
import { style, transition, trigger, animate } from '@angular/animations';

@Component({
  selector: 'app-loading-icon',
  templateUrl: './loading-icon.component.html',
  styleUrls: ['./loading-icon.component.css'],
  animations: [
    trigger(
      'outAnimation', 
      [
        transition(
          ':leave', 
          [
            style({ opacity: 1 }),
            animate('1s ease-in', 
                    style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class LoadingIconComponent {
  constructor(public data: DataServiceService) {
  }
}
