import { Component } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';
import { User } from '../services/data-service.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  clicked: boolean = false;
  newUser: boolean = false;
  result: string = '';
  stp = (event: { stopPropagation: () => any; })=>event.stopPropagation();

  login: string = '';
  password: string = '';
  emailRegex: RegExp = /^.*@.*$/;

  constructor(public user: UserServiceService) {
    user.user.subscribe(()=>{
      this.clicked = false;
    })
  }

  orangeIf(flag: boolean) {
    if (flag) {
      return 'rgb(217, 118, 37)';
    }
    return 'inherit';
  }

  logIn() {
    this.result = this.user.logIn(this.login, this.password);
  }

  
  signUp(user: User) {
    this.user.signUp(user);
    this.clicked=!this.clicked
  }
}
