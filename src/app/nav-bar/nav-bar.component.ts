import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';
import { Agency, DataServiceService, Destination } from '../services/data-service.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent {
  @Input() destination: Destination | undefined = undefined;
  @Input() agency: Agency | undefined = undefined;
  
  constructor(public user: UserServiceService, public data: DataServiceService) {
    
   }

   
}
