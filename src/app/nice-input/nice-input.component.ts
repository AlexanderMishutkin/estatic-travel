import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-nice-input',
  templateUrl: './nice-input.component.html',
  styleUrls: ['./nice-input.component.css']
})
export class NiceInputComponent {
  @Input() value: string = "";
  @Input() icon: string = "";
  @Input() regex: RegExp = /.*/;
  @Input() placeholder: string = "Enter value";
  @Input() error: string = "Wrong value!";
  @Input() type: string = "text";
  @Input() label: string = "";
  @Input() dateParams: {min: string,max: string} | undefined;

  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() valid: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('input') input?: ElementRef;

  validate(value: string) {
    this.valueChange.emit(value);
    if (this.regex.test(value)) {
      console.log('ok');
      if (this.input) {
        this.input.nativeElement.style.color = 'inherit';
      }
      this.valid.emit(true);
      return true;
    }
    else {
      console.log('bad');
      if (this.input) {
        this.input.nativeElement.style.color = 'rgb(217, 118, 37)';
      }
      this.valid.emit(false);
      return false;
    }
  }
}
