import { Injectable } from '@angular/core';
import { Observable, catchError, forkJoin, of, tap, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const firebase = 'https://estatic-travel-default-rtdb.europe-west1.firebasedatabase.app/'


export interface Destination {
  naziv: string,
  opis: string,
  slike: string[],
  tip: string,
  prevoz: string,
  cena: number,
  maxOsoba: number,
  id: string
}

export interface Agency {
  naziv: string,
  adresa: string,
  godina: number,
  logo: string,
  brojTelefona: string,
  email: string,
  destinacije: Destination[],
  id: string
}

export interface User {
  korisnickoIme: string,
  lozinka: string,
  ime: string,
  prezime: string,
  email: string,
  datumRodjenja: string,
  adresa: string,
  telefon: string,
  id: string
}

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  addUser(user: User) {
    this.users.push(user);
  }
  changeUser(user: User) {
    this.users[this.users.findIndex(u=>u.id == user.id)] = user;
  }
  constructor(private http: HttpClient) {
    this.http.get(firebase + '.json').subscribe((data) => {
      console.log(data);

      const { agencjie, destinacije, korisnici } = data as unknown as any;
      this.users = Object.keys(korisnici).map(key => ({
        ...(korisnici as unknown as { [key: string]: User })[key],
        id: key
      }));

      this.agencies = Object.keys(agencjie).map(key => ({
        ...(agencjie as unknown as { [key: string]: any })[key],
        id: key
      })).map(obj => {
        const ds = destinacije as unknown as { [key: string]: { [key: string]: any } };
        const curDs = (ds[obj.destinacije] as unknown as { [key: string]: any });
        return {
          ...obj,
          godina: +obj.godina,
          destinacije: Object.keys(curDs).map(d => ({
            ...curDs[d],
            cena: +curDs[d].cena,
            maxOsoba: +curDs[d].maxOsoba,
            id: d
          }))
        };
      });
      this.loading.next(false);
    })
  }

  users: User[] = [];
  agencies: Agency[] = [];

  loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  preloadImages(): Observable<any> {
    // var imageUrls = this.agencies.map(a => a.logo);
    // // for (var a of this.agencies) {
    // //   for (var d of a.destinacije) {
    // //     imageUrls = imageUrls.concat(...d.slike);
    // //   }
    // // }
    // const requests = imageUrls.map(url => this.http.get(url, { responseType: 'blob' }).pipe(catchError(err => of(undefined))));
    // console.log(imageUrls);
    // return forkJoin(requests).pipe(tap(console.log));
    return forkJoin([]);
  }

  addAgency(newAgency: Agency) {
    this.agencies.push(newAgency);
  }

  updateAgency(id: string, newAgency: Agency) {

  }

  deleteAgency(id: string) {
    this.agencies = this.agencies.filter(a => a.id != id);
  }


  addDestination(newDestination: Destination, toAgency: Agency) {
    toAgency.destinacije.push(newDestination);
  }

  updateDestination(id: string, newDestination: Destination, fromAgency: Agency) {
    fromAgency.destinacije[fromAgency.destinacije.findIndex(d => d.id == id)] = newDestination;
  }

  deleteDestination(id: string, fromAgency: Agency) {
    console.log(id, fromAgency)
    fromAgency.destinacije = fromAgency.destinacije.filter(a => a.id != id);
  }
}




// npm install firebase
