import { Injectable } from '@angular/core';
import { Observable, catchError, forkJoin, of, tap, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchServiceService {
  request: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null)

  constructor() { }

  amISimilar = (me: string) => {
    if (!this.request.value || this.request.value.length < 2 || !me || me.length < 2) {
      return false;
    }
    
    const words = this.request.value.split(' ');
    const minDif = Math.min(...words.map(a => 
      Math.min(...me.split(' ').map(b =>{
        let minDistance = this.LevenshteinDistance(a.toLowerCase(), b.toLowerCase());
        for (let i = 0; i < (b.length - a.length); i++) {
          let substring = b.substring(i, i + a.length);
          let distance = this.LevenshteinDistance(a.toLowerCase(), substring.toLowerCase());
          minDistance = Math.min(minDistance, distance);
        }
        return minDistance; 
      }
      ))
    ));

    return minDif < 2;
  }
  

  LevenshteinDistance(a: string, b: string) {
    if (a.length == 0) return b.length;
    if (b.length == 0) return a.length;

    var matrix = [];

    // increment along the first column of each row
    var i;
    for (i = 0; i <= b.length; i++) {
      matrix[i] = [i];
    }

    // increment each column in the first row
    var j;
    for (j = 0; j <= a.length; j++) {
      matrix[0][j] = j;
    }

    // Fill in the rest of the matrix
    for (i = 1; i <= b.length; i++) {
      for (j = 1; j <= a.length; j++) {
        if (b.charAt(i - 1) == a.charAt(j - 1)) {
          matrix[i][j] = matrix[i - 1][j - 1];
        } else {
          matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1, // substitution
            Math.min(matrix[i][j - 1] + 1, // insertion
              matrix[i - 1][j] + 1)); // deletion
        }
      }
    }

    return matrix[b.length][a.length];
  }
}
