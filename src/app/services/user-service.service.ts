import { Injectable } from '@angular/core';
import { DataServiceService, User } from './data-service.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  user: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null)

  constructor(private data: DataServiceService, private router: Router) { }

  logIn = (login: string, password: string) => {
    let user = this.data.users.find(user=>user.email == login)
    console.log(login, password, user, this.data.users)
    if (!user) {
      return 'No such user!';
    }
    if (user.lozinka != password) {
      return 'Wrong username and/or password!'
    }
    this.user.next(user);
    return '';
  }

  logOut = () => {this.user.next(null);}

  signUp = (user: User) => {
    this.data.addUser(user);
    this.user.next(user);
  }
}
