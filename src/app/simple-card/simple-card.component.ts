import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.css'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class SimpleCardComponent {
  @Input() image: string = '';
  @Input() caption: string = '';
  @Input() hashtags: string[] = [];
  @Input() description: string = '';
  @Input() goto?: string;
  @Input() editParams: {} = {};

  @Input() editLink?: string;
  @Input() removeCallback?: () => void;

  @ViewChild('wrapDiv') wrapDiv?: ElementRef;
  @ViewChild('imgDiv') imgDiv?: ElementRef;
  @ViewChild('card') card?: ElementRef;

  ratio: number = 1;
  @Input() simple: boolean = false;

  constructor(private router: Router, public user: UserServiceService) {
  }

  ngOnInit() {
    const img = new Image();
    img.onload = () => {
      this.ratio = img.height / img.width;
    };
    img.src = this.image;
    this.ratio = img.height / img.width;

    setTimeout(this.roundCSize, 500);
  }

  onResize(event: { target: { innerWidth: number; }; }){
    setTimeout(this.roundCSize, 100);
  }

  roundCSize = () => {
    if (this.card && !this.simple) {
      const cardHeight = this.card.nativeElement.getBoundingClientRect().height;
      const cardWidth = this.card.nativeElement.getBoundingClientRect().width;
      this.card.nativeElement.style.height = `${Math.ceil(cardWidth * Math.ceil(1.0 * cardHeight / cardWidth) / 1.0)}px`;
    }
  }

  toggleState() {
    if (this.simple) return;

    if (this.wrapDiv && this.imgDiv && this.card) {
      this.card.nativeElement.style.transition = 'none';
      this.wrapDiv.nativeElement.style.transition = 'none';
      this.imgDiv.nativeElement.style.transform = `scale(1)`;
      // this.imgDiv.nativeElement.style.filter = 'blur(5px)';
      const rect = this.wrapDiv.nativeElement.getBoundingClientRect();
      const width = rect.width;
      const height = rect.height;
      const top = rect.top;
      const left = rect.left;

      // Set position and size properties
      this.wrapDiv.nativeElement.style.top = `${top}px`;
      this.wrapDiv.nativeElement.style.left = `${left}px`;
      this.wrapDiv.nativeElement.style.width = `${width}px`;
      this.wrapDiv.nativeElement.style.height = `${height}px`;
      const cardHeight = this.card.nativeElement.getBoundingClientRect().height;
      const cardWidth = this.card.nativeElement.getBoundingClientRect().width;
      this.card.nativeElement.style.height = `${cardHeight}px`;
      this.card.nativeElement.style.width = `${cardWidth}px`;
      this.card.nativeElement.style.padding = `${0}px`;
      this.card.nativeElement.style.color = `white`;
      this.wrapDiv.nativeElement.style.padding = '0';
      this.wrapDiv.nativeElement.style.position = 'fixed';
      this.wrapDiv.nativeElement.style['z-index'] = '3';

      setTimeout((
      ) => {
        // Set new position and size
        if (this.wrapDiv && this.imgDiv) {
          this.wrapDiv.nativeElement.style.transition = 'all 0.5s ease-in-out';
          this.imgDiv.nativeElement.style.transition = 'all 0.5s ease-in-out';
          this.wrapDiv.nativeElement.style.top = '0';
          this.wrapDiv.nativeElement.style.left = '0';
          this.wrapDiv.nativeElement.style.width = '100vw';
          this.wrapDiv.nativeElement.style.height = '100vh';
          // this.imgDiv.nativeElement.style.padding = '100%';
          // this.imgDiv.nativeElement.style.height = '100vh';
        }

        setTimeout(() => {
          if (this.wrapDiv && this.imgDiv) {
            this.imgDiv.nativeElement.style.filter = 'blur(5px)';
          }
        }, 100)

        setTimeout(() => {
          this.router.navigateByUrl(this.goto ?? '/error')
        }, 500)
      }, 100)

    }
  }
}
