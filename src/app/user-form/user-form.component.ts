import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataServiceService, User } from '../services/data-service.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {
  userRegex: {[key: string]: RegExp};
  userError: {[key: string]: string};
  userPlaceholder: {[key: string]: string};

  @Input() user: User;

  result: string = '';
  @Output() newUser: EventEmitter<User> = new EventEmitter<User>();

  constructor(private data: DataServiceService) {
    this.user = {
      korisnickoIme: '',
      lozinka: '',
      ime: '',
      prezime: '',
      email: '',
      datumRodjenja: '',
      adresa: '',
      telefon: '',
      id: ''
    };
    this.userRegex = {
      korisnickoIme: /.+/,
      lozinka: /.{8,}/,
      ime: /.+/,
      prezime: /.+/,
      email: /.+@.+\..+/,
      telefon: /[+\d\-()/]+/,
      adresa: /.+/,
    };
    this.userError = {
      korisnickoIme: 'Name can not be empty',
      lozinka: 'Password must be 8+ cahracters length',
      ime: 'Name can not be empty',
      prezime: 'Surname can not be empty',
      email: 'Email must be valid email address',
      telefon: 'Phone must be a phone number',
      adresa: 'Address must be not empty',
    };
    this.userPlaceholder = {
      korisnickoIme: 'pera123',
      lozinka: '********',
      ime: 'Pera',
      prezime: 'Peric',
      email: 'example@example.com',
      adresa: '21000, Novi Sad, Bulevar Oslobodenja 3a, 5',
      telefon: '+ 7 (977) 977 27 20',
      id: ''
    };
  }

  signUp() {
    let flag = true;
    for (let key in this.userRegex) {
      if (!this.userRegex[key].test((this.user as unknown as {[key:string]:string})[key])) {
        console.log('fail!', this.userRegex[key], (this.user as unknown as {[key:string]:string})[key])
        this.result = this.userError[key]
        flag = false;
        break;
      }
    }
    if (flag) {
      this.result = '';
      console.log(this.user);
      this.newUser.emit(this.user);
    }
  }
}
